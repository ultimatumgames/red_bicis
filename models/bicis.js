
var Bicicleta = function(id,color,modelo,ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function(){
    return "id: "+ this.id + " | color: "+this.color;
}

Bicicleta.allBicis = [];
Bicicleta.add = function(aBici){
    Bicicleta.allBicis.push(aBici);
}
Bicicleta.findById = function(aBiciId){
    var aBici = Bicicleta.allBicis.find( x => x.id == aBiciId  ) 
    if(!aBici)
        throw new Error(`No existe una bicicleta con el id ${aBiciId}`); 
    else 
        return aBici;
        
}

Bicicleta.removeById = function(aBiciId){
    Bicicleta.findById(aBiciId);
    for (var index = 0; index < Bicicleta.allBicis.length; index++) {
        if (Bicicleta.allBicis[index].id == aBiciId){
            console.log("si esta"+index);
            Bicicleta.allBicis.splice(index,1);
            break ;
        }
    }
}


var a = new Bicicleta(0,'azul','gw',[4.610212, -74.165907])
var b = new Bicicleta(1,'roja','gw',[4.615212, -74.169907])

Bicicleta.add(a);
Bicicleta.add(b);


module.exports = Bicicleta;